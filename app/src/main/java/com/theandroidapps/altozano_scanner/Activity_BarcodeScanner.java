package com.theandroidapps.altozano_scanner;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.theandroidapps.altozano_scanner.Utils.UserSession;
import com.theandroidapps.altozano_scanner.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Activity_BarcodeScanner extends AppCompatActivity {

    private CodeScanner mCodeScanner;

    private UserSession session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_scanner);


        session = new UserSession(Activity_BarcodeScanner.this);


        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //    window.setStatusBarColor(getResources().getColor(R.color.design_default_color_primary));
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        }

/*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));
        }*/

       /* Intent i = new Intent(Activity_BarcodeScanner.this, QRDetailsActivity.class);
        i.putExtra("marchantId", "1");
        i.putExtra("cameFrom", "QRMenu");
        startActivity(i);
        finish();*/


        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull Result result) {

                Log.e("QRDATA", result.getText());

                Intent i = new Intent(Activity_BarcodeScanner.this, QRDetailsActivity.class);
                i.putExtra("marchantId", result.getText());
                i.putExtra("cameFrom", "QRMenu");
                startActivity(i);
                finish();

        //        barcodeCheck(result.getText());

            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });


        findViewById(R.id.closeScanne).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }



    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    public void onResume() {
        super.onResume();

        mCodeScanner.startPreview();

        UserSession userSession = new UserSession(Activity_BarcodeScanner.this);
        setLocale(userSession.getLanguageCode());
    }


    private void barcodeCheck(String code) {
        final KProgressHUD progressDialog = KProgressHUD.create(Activity_BarcodeScanner.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "" + code,
                new Response.Listener<NetworkResponse>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("barcodeScan", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                Intent i = new Intent(Activity_BarcodeScanner.this, QRDetailsActivity.class);
                                i.putExtra("marchantId", code);
                                i.putExtra("cameFrom", "QRMenu");
                                startActivity(i);
                                finish();

                            } else if (jsonObject.getString("ResponseCode").equals("422")){
                                progressDialog.dismiss();

                                Toast.makeText(Activity_BarcodeScanner.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                finish();

                            }



                        } catch (Exception e) {

                            Toast.makeText(Activity_BarcodeScanner.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(Activity_BarcodeScanner.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Activity_BarcodeScanner.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Activity_BarcodeScanner.this, R.string.bad_network_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //        params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(Activity_BarcodeScanner.this).add(volleyMultipartRequest);
    }





}
