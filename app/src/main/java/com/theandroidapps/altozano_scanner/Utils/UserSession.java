package com.theandroidapps.altozano_scanner.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

public class UserSession {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    public String BASEURL = "https://qrontool.com/altozano/api/";
    public String ANDROID_APP_VERSION = "1.0";

    private static final String PREF_NAME = "UserSessionPref";


    private static final String IS_LOGIN = "IsLoggedIn";
    private final String USER_ID = "User_id";
    private final String DEVICE_TOKEN = "device_token";
    private final String APITOKEN = "api_token";

    private static final String LANGUAGECODE = "languagecode";



    public UserSession(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }



    public boolean logout() {
        return sharedPreferences.edit().clear().commit();
    }


    public void setLanguageCode(String name){
        editor.putString(LANGUAGECODE, name);
        editor.commit();
    }

    public String getLanguageCode(){
        return sharedPreferences.getString(LANGUAGECODE,"es");
    }



    public void setAPITOKEN(String apitoken) {
        editor.putString(APITOKEN, apitoken);
        editor.commit();
    }

    public String getAPITOKEN() {
        return sharedPreferences.getString(APITOKEN, "");
    }




    public void setUSER_ID(String user_id) {
        editor.putString(USER_ID, user_id);
        editor.commit();
    }

    public String getUSER_ID() {
        return sharedPreferences.getString(USER_ID, "");
    }



    public void stayLoggedIn(boolean b) {
        editor.putBoolean(IS_LOGIN, b);
        editor.commit();
    }

    public boolean getStayLogIn(){
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }





    public String trimMessage(String json, String key){
        String trimmedString = null;
        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }


}
