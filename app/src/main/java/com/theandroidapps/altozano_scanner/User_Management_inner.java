package com.theandroidapps.altozano_scanner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.theandroidapps.altozano_scanner.Adapter.AdapterUser;
import com.theandroidapps.altozano_scanner.Adapter.SelectCitySpinner;
import com.theandroidapps.altozano_scanner.Model.CityModel;
import com.theandroidapps.altozano_scanner.Model.LogModel;
import com.theandroidapps.altozano_scanner.Utils.EndlessRecyclerViewScrollListener;
import com.theandroidapps.altozano_scanner.Utils.UserSession;
import com.theandroidapps.altozano_scanner.Utils.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class User_Management_inner extends AppCompatActivity {

    private RecyclerView recLog;
    private AdapterUser adapterUser;
    private Spinner spinnerUser;

    private ArrayList<CityModel> userArrayList = new ArrayList<>();
    private ArrayList<LogModel> logModelArrayList = new ArrayList<>();

    private UserSession session;

    private LinearLayoutManager linearlayout;
    private String mPage = "1";
    private int last_size;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_management_inner);

        session = new UserSession(User_Management_inner.this);



        recLog = findViewById(R.id.recLog);
        linearlayout = new LinearLayoutManager(User_Management_inner.this);
        recLog.setLayoutManager(linearlayout);
        adapterUser = new AdapterUser(User_Management_inner.this, logModelArrayList, new AdapterUser.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(User_Management_inner.this, New_User.class)
                .putExtra("user_id", logModelArrayList.get(item).getQr_codes_id()));
            }
        });
        recLog.setAdapter(adapterUser);

        recLog.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearlayout) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.e("PageStatus", page + "  " + last_size);
                if (page != last_size) {
                    mPage = String.valueOf(page + 1);
                    getByFilter("", mPage);
                }
            }
        });



        CityModel cityModel1 = new CityModel();
        cityModel1.setSortBy(getString(R.string.username_ascending));
        cityModel1.setValue("username_asc");
        userArrayList.add(cityModel1);

        CityModel cityModel = new CityModel();
        cityModel.setSortBy(getString(R.string.username_descending));
        cityModel.setValue("username_desc");
        userArrayList.add(cityModel);

        CityModel cityModel2 = new CityModel();
        cityModel2.setSortBy(getString(R.string.name_ascending));
        cityModel2.setValue("fullname_asc");
        userArrayList.add(cityModel2);

        CityModel cityModel3 = new CityModel();
        cityModel3.setSortBy(getString(R.string.name_descending));
        cityModel3.setValue("fullname_desc");
        userArrayList.add(cityModel3);

        CityModel cityModel12 = new CityModel();
        cityModel12.setSortBy(getString(R.string.date_asc));
        cityModel12.setValue("date_asc");
        userArrayList.add(cityModel12);

        CityModel cityMode1l3 = new CityModel();
        cityMode1l3.setSortBy(getString(R.string.date_desc));
        cityMode1l3.setValue("date_desc");
        userArrayList.add(cityMode1l3);

        CityModel cityModel11 = new CityModel();
        cityModel11.setSortBy(getString(R.string.filter_by));
        cityModel11.setValue("");
        userArrayList.add(cityModel11);



        spinnerUser = findViewById(R.id.spinnerUser);

        SelectCitySpinner adapter = new SelectCitySpinner(User_Management_inner.this, android.R.layout.simple_spinner_item, userArrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinnerUser.setAdapter(adapter);
        spinnerUser.setSelection(adapter.getCount());


        spinnerUser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(i != userArrayList.size()-1) {

                    logModelArrayList.clear();
                    getByFilter(userArrayList.get(i).getValue(), "1");

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    private void getByFilter(String filter_type, String mPage) {
        final KProgressHUD progressDialog = KProgressHUD.create(User_Management_inner.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "users?sort_by=" + filter_type
                + "&page=" + mPage,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){


                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                last_size = jsonObject1.getInt("last_page");

                                JSONArray jsonArray = jsonObject1.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++){

                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                    LogModel logModel = new LogModel();
                                    logModel.setQr_codes_id(jsonObject2.getString("user_id"));
                                    logModel.setFullname(jsonObject2.getString("fullname"));
                                    logModel.setUsername(jsonObject2.getString("username"));

                                    logModelArrayList.add(logModel);
                                }

                                adapterUser.notifyDataSetChanged();


                            } else if (jsonObject.getString("ResponseCode").equals("422")){
                                progressDialog.dismiss();

                                Toast.makeText(User_Management_inner.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                            }



                        } catch (Exception e) {

                            Toast.makeText(User_Management_inner.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(User_Management_inner.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(User_Management_inner.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(User_Management_inner.this, R.string.bad_network_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //        params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(User_Management_inner.this).add(volleyMultipartRequest);
    }


    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    public void onResume() {
        super.onResume();

        logModelArrayList.clear();
        getByFilter("", "1");

        UserSession userSession = new UserSession(User_Management_inner.this);
        setLocale(userSession.getLanguageCode());
    }




}