package com.theandroidapps.altozano_scanner.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theandroidapps.altozano_scanner.Model.LogModel;
import com.theandroidapps.altozano_scanner.R;

import java.util.ArrayList;

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContect;
    private ArrayList<LogModel> logModelArrayList;


    public AdapterUser(Context mContect, ArrayList<LogModel> logModelArrayList, OnItemClickListener listener) {
        this.mContect = mContect;
        this.listener = listener;
        this.logModelArrayList = logModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContect);
        View view = inflater.inflate(R.layout.adapter_user, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        holder.full_name.setText(logModelArrayList.get(position).getFullname());
        holder.user_name.setText(logModelArrayList.get(position).getUsername());

    }

    @Override
    public int getItemCount() {
        return logModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView full_name, user_name;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            full_name = itemView.findViewById(R.id.full_name);
            user_name = itemView.findViewById(R.id.user_name);

        }

    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}
