package com.theandroidapps.altozano_scanner.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.theandroidapps.altozano_scanner.Model.LogModel;
import com.theandroidapps.altozano_scanner.R;
import com.theandroidapps.altozano_scanner.User_Management_inner;
import com.theandroidapps.altozano_scanner.Utils.UserSession;

import java.util.ArrayList;
import java.util.Locale;

public class AdapterLog extends RecyclerView.Adapter<AdapterLog.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContect;
    private ArrayList<LogModel> logModelArrayList;


    public AdapterLog(Context mContect, ArrayList<LogModel> logModelArrayList, OnItemClickListener listener) {
        this.mContect = mContect;
        this.listener = listener;
        this.logModelArrayList = logModelArrayList;

        UserSession userSession = new UserSession(mContect);
        setLocale(userSession.getLanguageCode());

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContect);
        View view = inflater.inflate(R.layout.adapter_log, parent, false);
        return new Viewholder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });
        
        holder.name.setText(logModelArrayList.get(position).getName());
        holder.visitors_type.setText(logModelArrayList.get(position).getType_of_identification());
        holder.plates_no.setText(mContect.getString(R.string.vehicle_no) + logModelArrayList.get(position).getPlates_number());
        holder.address.setText(logModelArrayList.get(position).getAddress());


    }

    @Override
    public int getItemCount() {
        return logModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        
        TextView name, visitors_type, plates_no, address;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            visitors_type = itemView.findViewById(R.id.visitors_type);
            plates_no = itemView.findViewById(R.id.plates_no);
            address = itemView.findViewById(R.id.address);

        }

    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = mContect.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}
