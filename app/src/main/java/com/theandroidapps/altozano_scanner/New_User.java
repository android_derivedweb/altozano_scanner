package com.theandroidapps.altozano_scanner;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.theandroidapps.altozano_scanner.Utils.UserSession;
import com.theandroidapps.altozano_scanner.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

public class New_User extends AppCompatActivity {


    private EditText userNameET, passwordET, fullNameET;
    private UserSession session;

    private String android_id = "";
    private boolean isUpdateUser = false;

    private String user_id = "";


    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);

        session = new UserSession(New_User.this);

        userNameET = findViewById(R.id.userNameET);
        passwordET = findViewById(R.id.passwordET);
        fullNameET = findViewById(R.id.fullNameET);


        android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("android_id", android_id + "--");

        if (!getIntent().getStringExtra("user_id").equals("")){
            isUpdateUser = true;
            user_id = getIntent().getStringExtra("user_id");
            getUserData(user_id);
        }


        passwordET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    if (isUpdateUser) {
                        passwordET.setText("");
                    }
                }
            }
        });


        findViewById(R.id.saveBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (userNameET.getText().toString().isEmpty()){
                    Toast.makeText(New_User.this, R.string.enter_username, Toast.LENGTH_SHORT).show();
                } else if (passwordET.getText().toString().isEmpty()){
                    Toast.makeText(New_User.this, R.string.enter_password, Toast.LENGTH_SHORT).show();
                } else if (passwordET.getText().toString().length() < 8){
                    Toast.makeText(New_User.this, R.string.pass_must_8_length, Toast.LENGTH_SHORT).show();
                } else if (passwordET.getText().toString().contains(" ")){
                    Toast.makeText(New_User.this, R.string.remove_space, Toast.LENGTH_SHORT).show();
                } else if (fullNameET.getText().toString().isEmpty()){
                    Toast.makeText(New_User.this, R.string.enter_full_name, Toast.LENGTH_SHORT).show();
                } else {

                    if (isUpdateUser){
                        updateUser(user_id);
                    } else {
                        saveUser();
                    }
                }
            }
        });

        findViewById(R.id.discardBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }


    private void getUserData(String userId) {
        final KProgressHUD progressDialog = KProgressHUD.create(New_User.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "get-user-details?user_id=" + userId,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                userNameET.setText(jsonObject.getJSONObject("data").getString("username"));
                                fullNameET.setText(jsonObject.getJSONObject("data").getString("fullname"));
                                passwordET.setText("********");


                            } else if (jsonObject.getString("ResponseCode").equals("422")){
                                progressDialog.dismiss();

                                Toast.makeText(New_User.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                            }



                        } catch (Exception e) {

                            Toast.makeText(New_User.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(New_User.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(New_User.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(New_User.this, R.string.bad_network_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", userNameET.getText().toString());
                params.put("password", passwordET.getText().toString());
                params.put("fullname", fullNameET.getText().toString());
                params.put("device_type", "android");
                params.put("device_token", android_id);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
        //        params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(New_User.this).add(volleyMultipartRequest);
    }


    private void saveUser() {
        final KProgressHUD progressDialog = KProgressHUD.create(New_User.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.BASEURL + "new-user",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                Toast.makeText(New_User.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                finish();


                            } else if (jsonObject.getString("ResponseCode").equals("422")){
                                progressDialog.dismiss();

                                Toast.makeText(New_User.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                            }



                        } catch (Exception e) {

                            Toast.makeText(New_User.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(New_User.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(New_User.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(New_User.this, R.string.bad_network_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", userNameET.getText().toString());
                params.put("password", passwordET.getText().toString());
                params.put("fullname", fullNameET.getText().toString());
                params.put("device_type", "android");
                params.put("device_token", android_id);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
        //        params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(New_User.this).add(volleyMultipartRequest);
    }


    private void updateUser(String user_id) {
        final KProgressHUD progressDialog = KProgressHUD.create(New_User.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        String passwordText = "";

        if (passwordET.getText().toString().contains("*")){
            passwordText = "";
        } else {
            passwordText = passwordET.getText().toString();
        }

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.PATCH, session.BASEURL
                + "update-user?user_id=" + user_id + "&fullname=" + fullNameET.getText().toString()
                + "&username=" + userNameET.getText().toString()
                + "&password=" + passwordText,

                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                Toast.makeText(New_User.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                finish();


                            } else if (jsonObject.getString("ResponseCode").equals("422")){
                                progressDialog.dismiss();

                                Toast.makeText(New_User.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                            }



                        } catch (Exception e) {

                            Toast.makeText(New_User.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(New_User.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(New_User.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(New_User.this, R.string.bad_network_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
        //        params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(New_User.this).add(volleyMultipartRequest);
    }



    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    public void onResume() {
        super.onResume();

        UserSession userSession = new UserSession(New_User.this);
        setLocale(userSession.getLanguageCode());
    }



}