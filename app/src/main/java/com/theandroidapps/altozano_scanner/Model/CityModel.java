package com.theandroidapps.altozano_scanner.Model;


public class CityModel {

    String SortBy;
    String Value;



    public String getSortBy() {
        return SortBy;
    }

    public void setSortBy(String sortBy) {
        SortBy = sortBy;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }


}
