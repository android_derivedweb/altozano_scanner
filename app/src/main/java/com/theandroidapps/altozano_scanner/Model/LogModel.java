package com.theandroidapps.altozano_scanner.Model;


public class LogModel {

    String qr_codes_id;
    String name;
    String address;
    String type_of_identification;
    String plates_number;
    String username;
    String fullname;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getQr_codes_id() {
        return qr_codes_id;
    }

    public void setQr_codes_id(String qr_codes_id) {
        this.qr_codes_id = qr_codes_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType_of_identification() {
        return type_of_identification;
    }

    public void setType_of_identification(String type_of_identification) {
        this.type_of_identification = type_of_identification;
    }

    public String getPlates_number() {
        return plates_number;
    }

    public void setPlates_number(String plates_number) {
        this.plates_number = plates_number;
    }
}
