package com.theandroidapps.altozano_scanner;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.theandroidapps.altozano_scanner.Utils.UserSession;
import com.theandroidapps.altozano_scanner.Utils.VolleyMultipartRequest;

import org.json.JSONObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

public class QRDetailsActivity extends AppCompatActivity {

    private TextView discardbtn, saveBtn;
    private TextView addressET, nameET, typeIdentiET, numIdentiET, plates_numET, dateHoursET;

    private UserSession session;

    private String marchantId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_details);

        session = new UserSession(QRDetailsActivity.this);

        discardbtn = findViewById(R.id.discardbtn);
        saveBtn = findViewById(R.id.saveBtn);

        addressET = findViewById(R.id.addressET);
        nameET = findViewById(R.id.nameET);
        typeIdentiET = findViewById(R.id.typeIdentiET);
        numIdentiET = findViewById(R.id.numIdentiET);
        plates_numET = findViewById(R.id.plates_numET);
        dateHoursET = findViewById(R.id.dateHoursET);




        if (getIntent().getStringExtra("cameFrom").equals("LogData")){
            marchantId = getIntent().getStringExtra("marchantId");
            saveBtn.setVisibility(View.GONE);
            discardbtn.setVisibility(View.GONE);

            getUser("log");

        } else {
            marchantId = getIntent().getStringExtra("marchantId");
            getUser("logData");
        }



        findViewById(R.id.saveBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUser();
            }
        });


        findViewById(R.id.discardbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descardUser();
            }
        });


    }


    private void getUser(String scanValue) {
        final KProgressHUD progressDialog = KProgressHUD.create(QRDetailsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "scan-qr-code?qr_code_id=" + marchantId,
                new Response.Listener<NetworkResponse>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("scanResponce", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")){

                                addressET.setText(jsonObject.getJSONObject("data").getString("address"));
                                nameET.setText(jsonObject.getJSONObject("data").getString("name"));
                                typeIdentiET.setText(jsonObject.getJSONObject("data").getString("type_of_identification"));
                                numIdentiET.setText(jsonObject.getJSONObject("data").getString("number_of_identification"));
                                plates_numET.setText(jsonObject.getJSONObject("data").getString("plates_number"));

                            /*    DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-ddThh:mm:ss.SSSSSSZ", Locale.ENGLISH);
                                DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);

                                LocalDate date = LocalDate.parse(jsonObject.getJSONObject("data").getString("created_at"), inputFormatter);
                                String appDate = outputFormatter.format(date);*/
                                dateHoursET.setText(jsonObject.getJSONObject("data").getString("created_at"));

                                if (scanValue.equals("logData")){

                                    if (jsonObject.getJSONObject("data").getString("is_read").equals("1")){
                                        finish();
                                        Toast.makeText(QRDetailsActivity.this, "This QrCode is already scanned!", Toast.LENGTH_LONG).show();
                                    }

                                }


                            } else if (jsonObject.getString("ResponseCode").equals("422")){
                                progressDialog.dismiss();

                                Toast.makeText(QRDetailsActivity.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                finish();

                            }



                        } catch (Exception e) {

                            Toast.makeText(QRDetailsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(QRDetailsActivity.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(QRDetailsActivity.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(QRDetailsActivity.this, R.string.bad_network_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //        params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(QRDetailsActivity.this).add(volleyMultipartRequest);
    }


    private void saveUser() {
        final KProgressHUD progressDialog = KProgressHUD.create(QRDetailsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                session.BASEURL + "verify-qr-code-info?qr_code_id=" + marchantId,
                new Response.Listener<NetworkResponse>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("scanResponceSAVE", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")){


                                Toast.makeText(QRDetailsActivity.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                finish();


                            } else if (jsonObject.getString("ResponseCode").equals("422")){
                                progressDialog.dismiss();

                                Toast.makeText(QRDetailsActivity.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                            }



                        } catch (Exception e) {

                            Toast.makeText(QRDetailsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(QRDetailsActivity.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(QRDetailsActivity.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(QRDetailsActivity.this, R.string.bad_network_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
        //        params.put("qr_code_id", marchantId);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //        params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(QRDetailsActivity.this).add(volleyMultipartRequest);
    }

    private void descardUser() {
        final KProgressHUD progressDialog = KProgressHUD.create(QRDetailsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(getString(R.string.please_wait))
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.DELETE,
                session.BASEURL + "delete-qr-code-info?qr_code_id=" + marchantId,
                new Response.Listener<NetworkResponse>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            Log.e("scanResponceDELETE", jsonObject.toString() + "--");

                            if (jsonObject.getString("ResponseCode").equals("200")){


                                Toast.makeText(QRDetailsActivity.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                                finish();


                            } else if (jsonObject.getString("ResponseCode").equals("422")){
                                progressDialog.dismiss();

                                Toast.makeText(QRDetailsActivity.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();

                            }



                        } catch (Exception e) {

                            Toast.makeText(QRDetailsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(QRDetailsActivity.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(QRDetailsActivity.this, R.string.connection_timed_out, Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(QRDetailsActivity.this, R.string.bad_network_connection, Toast.LENGTH_LONG).show();

                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //        params.put("qr_code_id", marchantId);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //        params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(QRDetailsActivity.this).add(volleyMultipartRequest);
    }



    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    public void onResume() {
        super.onResume();

        UserSession userSession = new UserSession(QRDetailsActivity.this);
        setLocale(userSession.getLanguageCode());
    }



}